﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.SimpleFluent
{
    public class PersonFluentBuilder
    {
        private Person _person;

        public PersonFluentBuilder HasTitle(string title)
        {
            _person.Title = title;
            return this;
        }

        public PersonFluentBuilder HasFirstName(string firstName)
        {
            _person.FirstName = firstName;
            return this;
        }

        public PersonFluentBuilder HasLastName(string lastName)
        {
            _person.LastName = lastName;
            return this;
        }

        public PersonFluentBuilder HasAge(int age)
        {
            _person.Age = age;
            return this;
        }

        public PersonFluentBuilder()
        {
            this._person = new Person();
        }

        public Person Build()
        {
            return _person;
        }
    }
}
