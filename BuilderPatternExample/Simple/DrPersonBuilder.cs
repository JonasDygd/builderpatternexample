﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample
{
    public class DrPersonBuilder
    {
        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public int Age
        {
            get;
            set;
        }

        public Person Build()
        {
            return new Person
            {
                Title = "Dr.",
                FirstName = FirstName,
                LastName = LastName,
                Age = Age
            };
        }
    }
}
