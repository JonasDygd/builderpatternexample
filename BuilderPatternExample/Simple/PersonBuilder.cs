﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample
{
    public class PersonBuilder
    {
        public string Title
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public int Age
        {
            get;
            set;
        }

        public Person Build()
        {
            return new Person
            {
                Title = Title,
                FirstName = FirstName,
                LastName = LastName,
                Age = Age
            };
        }
    }
}
