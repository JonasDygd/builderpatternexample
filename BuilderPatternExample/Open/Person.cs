﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.Open
{
    public class Person
    {
        public string Title
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public int Age
        {
            get;
            set;
        }
    }
}
