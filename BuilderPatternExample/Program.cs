﻿using System;
using System.Collections.Generic;

namespace BuilderPatternExample
{
    class Program
    {
        public void Run()
        {            
            RunSimpleBuilder();
            RunSimpleButMoreSpecificBuilder();
            RunSimpleFluentBuilder();
            RunComplexFluentBuilder();

            Console.ReadKey();
        }

        private void RunSimpleBuilder()
        {
            // The simplest builder, with all properties exposed

            var personBuilder = new PersonBuilder();

            personBuilder.Title = "Dr.";
            personBuilder.FirstName = "Emmett";
            personBuilder.LastName = "Brown";
            personBuilder.Age = 40;

            Person person = personBuilder.Build();

            Console.WriteLine("Builder where all properties are exposed: ");
            WritePerson(person);
            Console.WriteLine();
        }

        private void RunSimpleButMoreSpecificBuilder()
        {
            // The DrPersonBuilder is a simple, but more specific builder, which will 
            // only create doctors.            

            // There is no Title property on the builder, it will always set Title 
            // to "Dr." on the Person object

            var personBuilder = new DrPersonBuilder();            

            personBuilder.FirstName = "Emmett";
            personBuilder.LastName = "Brown";
            personBuilder.Age = 40;

            Person person = personBuilder.Build();

            Console.WriteLine("Builder that only creates doctors: ");
            WritePerson(person);
            Console.WriteLine();
        }

        private void RunSimpleFluentBuilder()
        {
            // Now make use of the fluent builder pattern, which allows for chained method calls

            Person person = new SimpleFluent.PersonFluentBuilder().HasTitle("Dr.")
                                                                  .HasFirstName("Emmett")
                                                                  .HasLastName("Brown")
                                                                  .HasAge(40)
                                                                  .Build();

            Console.WriteLine("Fluent builder, with chained method calls: ");
            WritePerson(person);
            Console.WriteLine();
        }

        private void RunComplexFluentBuilder()
        {
            // This is also the fluent builder pattern with chained method calls. 

            // Here however, there are actually several sub-builder classes involved,
            // which guide the programmer to make the needed calls.

            // First, CreatePerson returns an instance of the PersonNameBeginningBuilder class

            Person person = CreatePerson().HasTitle("Dr.")          // returns the PersonNameBeginningBuilder as well, because call is optional
                                          .HasFirstName("Emmett")   // returns a PersonNameEndingBuilder, which only has the HasLastName method
                                          .HasLastName("Brown")     // returns a PersonAgeBuilder, which only has the HasAge method
                                          .HasAge(40)               // returns a PersonBuilder, which only has the Build method
                                          .Build();                 // only possible call at this point

            Console.WriteLine("Fluent builder, with chained method calls, and where the programmer is guided to make the needed calls: ");
            WritePerson(person);
            Console.WriteLine();
        }

        private ComplexFluent.PersonNameBeginningBuilder CreatePerson()
        {
            return new ComplexFluent.PersonNameBeginningBuilder();
        }        

        private void WritePerson(Person person)
        {
            var personNameParts = new List<string>();

            if (!string.IsNullOrWhiteSpace(person.Title))
                personNameParts.Add(person.Title);

            if (!string.IsNullOrWhiteSpace(person.FirstName))
                personNameParts.Add(person.FirstName);

            if (!string.IsNullOrWhiteSpace(person.LastName))
                personNameParts.Add(person.LastName);

            string personFullName = string.Join(" ", personNameParts); 

            Console.WriteLine(
                $"{personFullName} is {person.Age} years old."
                );
        }

        private static void Main()
        {
            new Program().Run();
        }
    }
}
