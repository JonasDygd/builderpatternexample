﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.ComplexFluent
{
    public class PersonNameEndingBuilder
    {
        private Person _person;

        public PersonAgeBuilder HasLastName(string lastName)
        {
            _person.LastName = lastName;
            return new PersonAgeBuilder(_person);
        }

        internal PersonNameEndingBuilder(Person person)
        {
            this._person = person;
        }
    }
}
