﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.ComplexFluent
{
    public class PersonAgeBuilder
    {
        private Person _person;

        public PersonBuilder HasAge(int age)
        {
            _person.Age = age;
            return new PersonBuilder(_person);
        }

        internal PersonAgeBuilder(Person person)
        {
            this._person = person;
        }
    }
}
