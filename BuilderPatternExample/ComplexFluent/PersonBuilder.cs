﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.ComplexFluent
{
    public class PersonBuilder
    {
        private Person _person;

        internal PersonBuilder(Person person)
        {
            this._person = person;
        }

        public Person Build()
        {
            return _person;
        }
    }
}
