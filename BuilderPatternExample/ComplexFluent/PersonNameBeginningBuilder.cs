﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.ComplexFluent
{
    public class PersonNameBeginningBuilder
    {
        private Person _person;

        public PersonNameBeginningBuilder HasTitle(string title)
        {
            _person.Title = title;
            return this;
        }

        public PersonNameEndingBuilder HasFirstName(string firstName)
        {
            _person.FirstName = firstName;
            return new PersonNameEndingBuilder(_person);
        }

        internal PersonNameBeginningBuilder()
        {
            this._person = new Person();
        }
    }
}
