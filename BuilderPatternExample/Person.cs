﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample
{
    public class Person
    {
        public string Title
        {
            get;
            internal set;
        }

        public string FirstName
        {
            get;
            internal set;
        }

        public string LastName
        {
            get;
            internal set;
        }

        public int Age
        {
            get;
            internal set;
        }
    }
}
